{{
  config(
    materialized='fastship_delta',
    schema= 'stg'
  )
}}

SELECT returns.RETURNED_FLAG AS RETURNED_FL, returns.ORDER_COD AS ORDER_CD, returns.PRODUCT_COD AS PRODUCT_CD
FROM csv.returns
