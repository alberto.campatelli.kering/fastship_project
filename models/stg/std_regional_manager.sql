{{
  config(
    materialized='fastship_delta',
    schema= 'stg'
  )
}}

SELECT regional_manager.REGION AS REGION_DS, regional_manager.MARKET AS MARKET_DS, regional_manager.PERSON AS MANAGER_DS
FROM csv.regional_manager
