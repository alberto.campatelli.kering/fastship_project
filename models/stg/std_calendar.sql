{{
  config(
    materialized='fastship_delta',
    schema= 'stg'
  )
}}

SELECT calendar.DATE_DT, calendar.HALF_CODE AS HALF_CD, calendar.QUARTER_CODE AS QUARTER_CD, calendar.YEAR_CODE AS YEAR_CD, calendar.MONTH_CODE AS MONTH_CD,
       calendar.MONTH_DESC AS MONTH_DS, calendar.WEEK_CODE AS WEEK_CD, calendar.WEEK_DESC AS WEEK_DS, calendar.DAY_CODE AS DAY_CS, calendar.DAY_DESC AS DAY_DS,
       calendar.DATE_CODE AS DATE_CD, calendar.DATE_DESC AS DATE_DS
FROM csv.calendar
