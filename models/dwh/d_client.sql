{{
  config(
    materialized='fastship_delta',
    schema= 'dwh'
  )
}}

SELECT DISTINCT nextval('client_sq') AS CLIENT_ID, CLIENT_CD, CLIENT_DS, CLIENT_SEGMENT AS SEGMENT_DS, ADDRESS_ID
FROM {{ source('stg', 'stf_orders') }} AS ord
LEFT JOIN {{ ref('d_address') }} AS add
ON ord.CLIENT_ADDRESS_COD = add.ADDRESS_CD
