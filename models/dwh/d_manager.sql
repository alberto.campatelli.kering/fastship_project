{{
  config(
    materialized='fastship_delta',
    schema= 'dwh'
  )
}}

SELECT nextval('regional_manager_sq') AS REGIONAL_MANAGER_ID, REGION_DS, MARKET_DS, MANAGER_DS
FROM {{ source('stg', 'std_regional_manager') }}
