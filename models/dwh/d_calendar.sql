{{
  config(
    materialized='fastship_delta',
    schema= 'dwh'
  )
}}

SELECT nextval('date_sq') AS DATE_ID, DATE_DT, HALF_CD, QUARTER_CD, YEAR_CD, MONTH_CD, MONTH_DS, WEEK_CD, WEEK_DS, DAY_CS, DAY_DS, DATE_CD, DATE_DS
FROM {{ source('stg', 'std_calendar') }}
