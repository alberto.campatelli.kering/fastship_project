{{
  config(
    materialized='fastship_delta',
    schema= 'dwh'
  )
}}

SELECT nextval('address_sq') AS ADDRESS_ID, ADDRESS_CD, COUNTRY_DS, STATE_DS, POSTAL_CODE_CD
FROM {{ ref('wrk_address') }}
