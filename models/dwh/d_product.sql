{{
  config(
    materialized='fastship_delta',
    schema= 'dwh'
  )
}}

SELECT nextval('product_sq') AS PRODUCT_ID, PRODUCT_CD, PRODUCT_DS, CATEGORY_DS, SUBCATEGORY_DS
FROM {{ source('stg', 'stf_orders') }}
